PR=1651
PV=1441
IX=1151
IY=$IX
TIME=

DIR=/tmp
BIN1=$DIR/omp/ctsimtext
BIN2=$DIR/sng/ctsimtext

for PHAN in herman; do
    fbase=${DIR}/${PHAN}
    rm -f ${fbase}-*
    
    echo -n "$BIN1 Prj: "; $TIME $BIN1 phm2pj $fbase-omp.pj $PR $PV --phantom $PHAN --verbose | tail -1  | sed 's/Run time: (.+) /$1/'
    echo -n "$BIN2 Prj: "; $TIME $BIN2 phm2pj $fbase-sng.pj $PR $PV --phantom $PHAN --verbose | tail -1 | sed 's/Run time: (.+) /$1/'

    for BP in idiff diff; do
        for INT in nearest linear cubic; do
            recbase=${fbase}-${BP}-${INT}
            echo -n "$BIN1 Rec $BP $INT: "; $TIME $BIN1 pjrec $fbase-omp.pj $recbase-omp.if $IX $IY --verbose --interp $INT --backproj $BP | tail -1
            echo -n "$BIN2 Rec $BP $INT: "; $TIME $BIN2 pjrec $fbase-sng.pj $recbase-sng.if $IX $IY --verbose --interp $INT --backproj $BP| tail -1
            echo -n "--> Difference Rec $BP $INT: "; $BIN1 if2 $recbase-omp.if $recbase-sng.if --comp
            echo
        done
    done

    phmbase=${fbase}-phm
    echo -n "$BIN1 Phm: "; $TIME $BIN1 phm2if ${phmbase}-omp.if $IX $IX --phantom $PHAN --nsample 5 --verbose | tail -1
    echo -n "$BIN2 Phm: "; $TIME $BIN2 phm2if ${phmbase}-sng.if $IX $IX --phantom $PHAN --nsample 5 --verbose | tail -1
    echo -n "--> Image diff Phantom: "; $BIN1 if2 ${phmbase}-omp.if ${phmbase}-sng.if --comp
    echo
done
